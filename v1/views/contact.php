<?php
$page_title = "Contact Us";

$error= [];

if(array_key_exists('submit', $_POST)){
   if(empty($error)){
     $to = $site_email;
     $subject = "Message from justinches.us";


     require APP_PATH.'/phpm/PHPMailerAutoload.php';

     // If necessary, modify the path in the require statement below to refer to the
     // location of your Composer autoload.php file.
     // require 'phpm/PHPMailerAutoload.php';


     // Instantiate a new PHPMailer
     $mail = new PHPMailer;

     // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
     // $mail->SMTPDebug = 2; //Alternative to above constant
     // Tell PHPMailer to use SMTP
     $mail->isSMTP();

     // Replace sender@example.com with your "From" address.
     // This address must be verified with Amazon SES.
     $mail->setFrom($site_email, $site_name);
     // $mail->AddReplyTo($site_email, $site_name);

     // Replace recipient@example.com with a "To" address. If your account
     // is still in the sandbox, this address must be verified.
     // Also note that you can include several addAddress() lines to send
     // email to multiple recipients.
     $mail->addAddress($to);

     // Replace smtp_username with your Amazon SES SMTP user name.
     $mail->Username = $site_email;

     // Replace smtp_password with your Amazon SES SMTP password.
     $mail->Password = getenv("EMAIL_PASSWORD");
      // die(var_dump($mail->Password));

     // Specify a configuration set. If you do not want to use a configuration
     // set, comment or remove the next line.
     // $mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');

     // If you're using Amazon SES in a region other than US West (Oregon),
     // replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP
     // endpoint in the appropriate region.
     $mail->Host = 'smtp.gmail.com';

     // The subject line of the email
     $mail->Subject = $subject;

     // The HTML-formatted body of the email
     $mail->Body = "

     <p>Message from Justinches </p>";

 if (isset($_POST['name']) && !empty($_POST['name'])) {
     $mail->Body.="<p>Name: ".$_POST['name']."</p>";
 }
 if (isset($_POST['email']) && !empty($_POST['email'])) {
     $mail->Body.="<p>Email: ".$_POST['email']."</p>";
 }
 if (isset($_POST['subject']) && !empty($_POST['subject'])) {
     $mail->Body.="<p>Subject: ".$_POST['subject']."</p>";
 }
 if (isset($_POST['message']) && !empty($_POST['message'])) {
     $mail->Body.="<p>Message: ".$_POST['message']."</p>";
 }





     // Tells PHPMailer to use SMTP authentication
     $mail->SMTPAuth = true;

     // Enable TLS encryption over port 587
     $mail->SMTPSecure = 'tls';
     $mail->Port = 587;

     // Tells PHPMailer to send HTML-formatted email
     $mail->isHTML(true);

     // The alternative email body; this is only displayed when a recipient
     // opens the email in a non-HTML email client. The \r\n represents a
     // line break.
     $mail->AltBody = "Do not send a reply to this mail";

     if(!$mail->send()) {
    	 // die(var_dump($mail->Body));
    	 $_SESSION['failed'] = 'Message was not sent. Try again after some time.';
    	 header("Location:".$_SERVER['REQUEST_URI']);
    		//  exit();
    		// echo "Email not sent!, error occured" , PHP_EOL;
    		exit();
    			// die(var_dump("Email not sent. " , $mail->ErrorInfo , PHP_EOL));
     } else {
    		$_SESSION['success'] = 'Message Sent, Thank you for contacting us!.';
    		header("Location:".$_SERVER['REQUEST_URI']);
    		//  exit();
    		// echo "Email sent!" , PHP_EOL;
    		exit();
     }

   }

 }

// $contactDetails = selectContent($conn, "panel_contact", ['visibility' => "show"]);
$breadcrumb = selectContent($conn, "settings_contact_breadcrumb", ['visibility' => "show"])[0];
 ?>
 <?php include "includes/header.php"; ?>

<section>
      <div class="w-100 dark-layer3 opc85 position-relative">
            <div class="fixed-bg" style="background-image: url(<?php echo $breadcrumb['image_1'] ?>);"></div>
            <div class="container">
            <div class="page-title text-center w-100">
                  <h1 class="mb-0"><?php echo $breadcrumb['input_title'] ?><span class="thm-clr">.</span></h1>
            </div>
            <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/" title=""><i class="icon_house"></i></a></li>
                  <li class="breadcrumb-item active">Contact us</li>
            </ol>
            </div>
      </div>
</section>
<section>
      <div class="w-100 pb-120 position-relative">
            <!-- <div class="cont-map style2 w-100" id="cont-map"></div> -->
            <div class="container">
            <div class="get-touch-wrap style2 w-100">
                  <div class="row">
                        <div class="col-md-5 col-sm-12 col-lg-4">
                        <div class="get-info w-100 mt-5">
                              <h2 class="mb-0">Get In Touch</h2>
                              <p class="mb-0">If you are interested in working with us, please get in touch.</p>
                              <h4 class="pt-5">Contact Address (Nigeria)</h4>
                              <ul class="get-info-list d-inline-block mb-0 list-unstyled w-100">
                                <li><i class="icon_map_alt thm-clr"></i> <?= $site_address ?></li>
                                <li><i class="fas fa-phone-volume thm-clr"></i> <?= $site_phone; ?></li>
                                <li><i class="far fa-paper-plane thm-clr"></i><a href="javascript:void(0);" title=""> <?= $site_email ?></a></li>
                              </ul>
                              <h4 class="pt-5">Contact Address (US)</h4>
                              <ul class="get-info-list d-inline-block mb-0 list-unstyled w-100">
                                <li><i class="icon_map_alt thm-clr"></i> <?= $site_address2 ?> </li>
                                <li><i class="fas fa-phone-volume thm-clr"></i> <?= $site_phone2; ?></li>
                                <li><i class="far fa-paper-plane thm-clr"></i><a href="javascript:void(0);" title=""> <?= $site_email2 ?></a></li>
                              </ul>
                        </div>
                        </div>
                        <div class="col-md-7 col-sm-12 col-lg-8">
                        <div class="form-wrap w-100 mt-5">
                              <h2 class="mb-0">FeedBack</h2>

                              <?php //if(isset($messages)): ?>
                              <?php //foreach($messages as $key => $message): ?>
                                    <!-- <div class="alert alert-success mt-3" role="alert">
                                          <?php //echo $message; ?>
                                    </div> -->
                              <?php //endforeach; ?>
                              <?php //endif; ?>

                              <form method="POST" class="w-100">
                                    <div class="row">
                                    <div class="col-md-6 col-sm-6 col-lg-4">
                                          <input class="w-100 brd-rd3 mt-30" name="name" type="text" placeholder="Full Name">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-4">
                                          <input class="w-100 brd-rd3 mt-30" name="email" type="email" placeholder="Email *">
                                    </div>
                                    <div class="col-md-12 col-sm-6 col-lg-4">
                                          <input class="w-100 brd-rd3 mt-30" name="subject" type="text" placeholder="Subject">
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-lg-12">
                                          <textarea class="w-100 brd-rd3 mt-30" name="message" placeholder="Your Message"></textarea>
                                          <button class="thm-btn mini-btn brd-rd3 mt-50" name="submit" type="submit">SEND MESSAGE</button>
                                    </div>
                                    </div>
                              </form>
                        </div>
                        </div>
                  </div>
            </div>
            </div>
      </div>
</section>
<?php include "includes/footer.php"; ?>
