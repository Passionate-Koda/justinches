<?php
$page_title = "Digital Innovations";

$blog = selectContent($conn, "panel_blog", ['visibility' => "show"]);
$breadcrumb = selectContent($conn, "settings_blog_breadcrumb", ['visibility' => "show"])[0];
 ?>
<?php include "includes/header.php"; ?>

<section>
      <div class="w-100 dark-layer3 opc85 position-relative">
      <!-- "./../../../public/jsimages/about_us_page_main_image.jpg" -->
            <div class="fixed-bg" style="background-image: url(<?php echo $breadcrumb['image_1'] ?>);"></div>
            <div class="container">
            <div class="page-title text-center w-100">
                  <h1 class="mb-0"><?php echo $breadcrumb['input_title'] ?><span class="thm-clr">.</span></h1>
            </div>
            <!-- Page Title -->
            <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#" title=""><i class="icon_house"></i></a></li>
                  <li class="breadcrumb-item active">Blog</li>
            </ol>
            <!-- Breadcrumb -->
            </div>
      </div>
</section>


<section>
    <div class="w-100 pt-155 pb-120 position-relative">
        <div class="container">
            <div class="blog-wrap2 position-relative w-100">
                <div class="row">

                <?php foreach($blog as $post => $value): ?>
                    <div class="col-md-6 col-sm-6 col-lg-6">
                        <div class="post-style2 mb-45 w-100">
                            <div class="post-img2 position-relative overflow-hidden w-100"><a href="/readblog?id=<?php echo $value['hash_id'] ?>" title="">
                              <div class="" style="width: 100%;padding-top: 60%;background:url(<?php echo $value['image_1'] ?>);background-size:cover; background-repeat:no-repeat;background-position:center">
                            </div>
                              <!-- <img class="img-fluid w-100" src="/<?php echo $value['image_1'] ?>" alt="Post Image 1"> -->
                            </a></div>
                            <div class="post-info2 w-100">
                                <div class="post-meta thm-bg brd-rd3">
                                    <span><?php echo decodeDate($value['date_created']) ?></span>
                                </div>
                                <h3 class="mb-0"><a href="/readblog?id=<?php echo $value['hash_id'] ?>" title=""><?php echo $value['input_title']; ?></a></h3>
                                <p class="mb-0"><?php echo previewBody($value['text_content'], 30); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                </div>
            </div><!-- Blog Style 2 -->
            <!-- <div class="pagination-wrap mt-55 w-100 text-center">
                <ul class="pagination mb-0">
                    <li class="page-item"><a class="page-link" href="javascript:void(0);" title="">1</a></li>
                    <li class="page-item active"><span class="page-link">2</span></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);" title="">3</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);" title="">4</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);" title="">5</a></li>
                    <li class="page-item">...</li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);" title="">13</a></li>
                </ul>
            </div> -->
            <!-- Pagination Wrap -->
        </div>
    </div>
</section>




<?php include "includes/footer.php"; ?>
