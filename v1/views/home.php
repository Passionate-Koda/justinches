<?php
$page_title = "Home";

$slider = selectContent($conn, "panel_slider", ['visibility' => "show"]);
$homeData = selectContent($conn, "panel_home_portfolio", ['visibility' => "show"]);
$blog = selectContentDesc($conn, "panel_blog", ['visibility' => "show"], "id", 3);
$testimonial = selectContent($conn, "panel_testimonial", ['visibility' => "show"]);
$unique_services = selectContent($conn, "panel_home_unique_services", ['visibility' => "show"]);
$portfolioImages = selectContent($conn, "panel_portfolio_images", ['visibility' => "show"]);
$whatwedo = selectContent($conn, "panel_what_we_do", ['visibility' => "show"]);
$video = selectContent($conn, "settings_home_video", [])[0];
$collaborative = selectContent($conn, "settings_home_collaborative_section", [])[0];
$testimonialSection = selectContent($conn, "settings_home_testimonial_section", [])[0];
$portfolioSection = selectContent($conn, "settings_home_portfolio_section", [])[0];
$blogSection = selectContent($conn, "settings_home_blog_section", [])[0];
$whatwedoSection = selectContent($conn, "settings_home_whatwedo_section", [])[0];
 ?>
<?php include "includes/header.php"; ?>
<style type="text/css">
  * {box-sizing:border-box}

/* Slideshow container */
.slideshow-container {
/*max-width: 1000px;*/
position: relative;
margin: auto;
width: 100%;
}

/* Hide the images by default */
.mySlides {
display: none;
}

.dot {
cursor: pointer;
height: 15px;
width: 15px;
margin: 0 2px;
background-color: #bbb;
border-radius: 50%;
display: inline-block;
transition: background-color 0.6s ease;
}

/* .active, .dot:hover {
background-color: #717171;
} */

</style>
    <section>
        <div class="w-100" style="position: relative;">
        <!-- <section> -->
        <!-- <div class="slider" style="background-image: url("https://www.justinches.us/public/assets/js/slider-images/gradienta-gwE9vXSi7Xw-unsplash.jpg");"> -->
            <?php foreach ($slider as $key => $value): ?>
              <div class="mySlides slider" style="background: url('<?php echo $value['image_1'] ?>') #121318;background-size:cover;background-position: center;background-repeat: no-repeat;height:100vh">
                  <div class="slider--content">
                  <!-- <button class="slider__btn-left" onclick="plusSlides(-1)">
                      <i class="fas fa-angle-left"></i>
                  </button> -->
                  <div class="slider--feature pt-0">
                      <h1 class="slider--title" style="text-shadow: 1px 1px #000;"><strong><?php echo $value['input_title'] ?></strong> </h1>
                      <p class="slider--text" style="text-shadow: 1px 1px #000;"><strong><?php echo $value['input_text'] ?></strong> </p>
                      <div class="pt-2">
                        <a href="/<?php echo $value['input_link_url'] ?>" class="thm-btn mini-btn brd-rd3"><?php echo $value['input_link_name'] ?></a>
                      </div>
                  </div>
                  <!-- <button class="slider__btn-right" onclick="plusSlides(1)">
                      <i class="fas fa-angle-right"></i>
                  </button> -->
                  </div>
              </div>
              <!-- <div class="cta__btn-container pb-5">
                <a href="/contact"  class="btn cta__btn">Learn More</a>
                <a href="/contact"  class="btn cta__btn">Talk to us</a>
              </div> -->
              <div class="d-flex justify-content-center" style="text-align:center; position: absolute; top: 95%; left: 45%">
                <?php foreach ($slider as $key => $value): ?>
                  <span class="dot" onclick="currentSlide(<?php echo $key ?>)"></span>
                <?php endforeach; ?>
              </div>
            <?php endforeach; ?>

        </div>

    </section>

    <!-- <section class="mb-5"> </section> -->
    <section>

        <div class="what__we-do" style="width: 100%; background: <?php echo $whatwedoSection['bgcolor_section'] ?>">
            <div class="what__we__do-right">
                <ul class="mb-0 p-2">
                    <li><h4 style="color: <?php echo $whatwedoSection['textcolor_title'] ?>"><?php echo $whatwedoSection['input_title'] ?></h4></li>
                    <?php foreach ($whatwedo as $key => $value): ?>
                      <li><a href="<?php echo $value['input_link'] ?>">
                              <i class="<?php echo $value['icon_icon'] ?>" style="color: <?php echo $whatwedoSection['textcolor_text'] ?>"></i>
                              <p class="wwd mb-0" style="color: <?php echo $whatwedoSection['textcolor_text'] ?>"><?php echo $value['input_title'] ?></p>
                      </a>
                      </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

    </section>

    <section style="background: <?php echo $collaborative['bgcolor_section'] ?>">

    <div class="w-100 pt-90">
        <div class="container">
          <div class="w-100">
              <div class="sec-title text-center w-100 mb-55">
                  <div class="sec-title-inner d-inline-block">
                      <h2 class="mb-0" style="color: <?php echo $collaborative['textcolor_section_title'] ?>"><?php echo $collaborative['input_title'] ?></h2>
                      <span class="d-inline-block thm-clr" style="color: <?php echo $collaborative['textcolor_section_subtitle'] ?>"><?php echo $collaborative['input_sub_title'] ?></span>
                  </div>
              </div>
          </div>
            <div class=" swiper mySwiper  text-center">
                <div class="swiper-wrapper ">
                  <?php foreach ($unique_services as $key => $value): ?>
                    <div class="swiper-slide col-md-4 col-sm-6 col-lg-4" id="dSwitch" >
                        <div class="srv-box1 mb-30 w-100 brd-rd10"  style="height:230px">
                          <h1 class="<?php echo $value['icon_icon'] ?>"></h1>
                            <!-- <img class="img-fluid" src="/public/jsimages/61f6fc0247cbb.png" alt="Service Icon 1"> -->
                            <div class="srv-info1 mt-30 w-100">
                                <h3 class="mb-0" style="color: <?php echo $collaborative['textcolor_title'] ?>"><?php echo $value['input_title'] ?></h3>
                                <!-- <p class="mb-0 mt-20">Place lights there don't grass, may dry from. Us he, itself may saw beginning. Night upon, they're created.</p> -->
                            </div>
                        </div>
                    </div>
                  <?php endforeach; ?>
                </div>
        </div>
      </div>
      <!-- <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div> -->
      <!-- <div class="swiper-pagination"></div> -->
    </div>
    </section>

    <section style="background: <?php echo $collaborative['bgcolor_section'] ?>">
            <?php foreach ($unique_services as $key => $value): ?>
              <div class="w-100 pb-70 pt-70 position-relative bs" id="bs">
                  <div id="particles1" class="particles-js top-left" data-saturation="300" data-size="40" data-count="20" data-speed="2" data-hide="770" data-image="assets"></div>
                  <div class="container">
                      <div class="about-wrap w-100">
                          <div class="row align-items-center">
                              <div class="col-md-12 col-sm-12 col-lg-7">
                                  <div class="about-mckp slide-animation w-100">
                                      <img class="img-fluid" src="<?php echo $value['image_1'] ?>" alt="About Mockup 1">
                                  </div>
                              </div>
                              <div class="col-md-12 col-sm-12 col-lg-5">
                                  <div class="about-info w-100">
                                      <h2 class="mb-0" style="color: <?php echo $collaborative['textcolor_title'] ?>"><?php echo $value['input_title'] ?></h2>
                                      <p class="mb-0" style="color: <?php echo $collaborative['textcolor_text'] ?>"><?php echo $value['text_content'] ?></p>
                                      <a class="thm-btn mini-btn brd-rd3" href="/about" title="">ABOUT US</a>
                                  </div>
                              </div>
                          </div>
                      </div><!-- About Style 1 -->
                  </div>
                  </div>
            <?php endforeach; ?>

    </section>




<?php if($homeData): ?>
              <section style="margin-top: 50px; padding-top:50px;">
                  <div class="w-100 pxb-70 position-relative">
                      <!-- <div class="container"> -->
                          <div class="sec-title style2 text-center w-100 mb-55">
                              <div class="sec-title-inner d-inline-block">
                                  <h2 class="mb-0"><?php echo $portfolioSection['input_title'] ?></h2>
                                  <span class="d-inline-block"><?php echo $portfolioSection['input_sub_title'] ?></span>
                              </div>
                          </div>
                          <div class="text-center">

                              <div class="row masonrssy mrg">
                                  <?php foreach ($portfolioImages as $key => $value): ?>
                                    <div class="col-md-3" >
                                        <div class="port-box7 w-100 mdb-45">
                                            <div class="port-img7 position-relative overflow-hidden w-100">
                                              <?php if ($value['input_embed_link']): ?>
                                                <div class="video-pres-inner" style="position: absolute; top:35%; left: 35%">
                                                  <a href='<?php echo $value['input_embed_link'] ?>' data-fancybox title="Video"><i class="fas fa-play"></i></a>
                                                </div>
                                              <?php endif; ?>
                                                <a href='<?php echo $value['input_embed_link'] ? $value['input_embed_link'] : $value['image_1']; ?>' data-fancybox="gallery" title="">
                                                  <div style="background: url(<?php echo $value['image_1']; ?>); background-repeat: no-repeat; background-position: center; background-size: cover; padding-top: 90%">

                                                  </div>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                  <?php endforeach; ?>

                              </div>
                          </div>

                      <!-- </div> -->
                      <div class="port-wrap w-100">
                          <div class="row mrg">
                              <?php foreach($homeData as $key => $value): ?>
                              <div class="col-md-6 col-sm-6 col-lg-3">
                                  <div class="port-box1 bg-black position-relative overflow-hidden w-100">
                                      <img class="img-fluid w-100" src="<?php echo $value['image_1']; ?>" alt="Portfolio Image 1">
                                      <div class="port-info1 position-absolute w-100">
                                          <span class="text-color9"><a href="<?php echo $value['input_link_to_case_study'] ? $value['input_link_to_case_study'] : '#' ?>"><?php echo $value['input_title']; ?></a></span>
                                          <a href="<?php echo $value['input_link_to_case_study'] ? $value['input_link_to_case_study'] : '#' ?>" title=""><h6 class="mb-0 text-white"><?php echo $value['input_subtitle']; ?></h6></a>
                                      </div>
                                      <div class="port-bottom-info1 position-absolute w-100">
                                          <i class="bg-black position-absolute"></i>
                                          <span class="text-white"><?php echo $value['input_title'] ?></span>
                                          <!-- <h6 class="mb-0 text-white"><?php echo $value['input_subtitle']; ?></h6> -->
                                      </div>
                                  </div>
                              </div>
                              <?php endforeach ?>
                          </div>
                      </div>
                      <!-- Portfolio Style 1 -->
                  </div>
              </section>


  <?php endif; ?>
    <section>
        <div class="w-100 pb-155 pt-180 black-layer opc65 position-relative">
            <div class="fixed-bg" style="background-image: url(<?php echo $video['image_1'] ?>);"></div>
            <div class="container">
            <div class="video-pres-wrap text-center w-100">
                    <div class="video-pres-inner d-inline-block">
                        <a href='<?php echo $video['input_embed_link'] ?>' data-fancybox title="Video"><i class="fas fa-play"></i></a>
                        <!-- <span class="d-block">How Can We Help ?</span> -->
                        <h3 class="mb-0" style="color: <?php echo $video['textcolor_text'] ?>"><?php echo $video['input_text'] ?></h3>
                        <a class="thm-btn mini-btn brd-rd3" href="<?php echo $video['input_link_url'] ?>" title=""><?php echo $video['input_link_name'] ?></a>
                    </div>
            </div>
            </div>
        </div>
    </section>



<?php if ($blog): ?>
  <section style="background: <?php echo $blogSection['bgcolor_section'] ?>">
      <div class="w-100 pt-130 pb-100 position-relative">
          <div class="container">
              <div class="sec-title text-center w-100 mb-55">
                  <div class="sec-title-inner d-inline-block">
                      <h2 class="mb-0" style="color: <?php echo $blogSection['textcolor_title'] ?>"><?php echo $blogSection['input_title'] ?></h2>
                      <span class="d-inline-block thm-clr" style="color: <?php echo $blogSection['textcolor_subtitle'] ?>"><?php echo $blogSection['input_sub_title'] ?></span>
                  </div>
              </div>
              <div class="blog-wrap position-relative w-100">
                  <?php foreach ($blog as $key => $value): ?>

                    <?php if ($value['id'] % 2 == 0): ?>
                      <div class="post-style1 rev mb-30 overflow-hidden d-flex flex-wrap brd-rd10 bg-color15">
                          <div class="post-img1"><a href="/readblog?id=<?php echo $value['hash_id'] ?>" title=""><img class="img-fluid" src="<?php echo $value['image_1'] ?>" alt="Post Image 1"></a></div>
                          <div class="post-info1 bg-color17">
                              <!-- <span class="post-cat d-block"><a class="bg-color18" href="javascript:void(0);" title="">Branding</a></span> -->
                              <h4 class="mb-0"><a href="/readblog?id=<?php echo $value['hash_id'] ?>" title=""><?php echo $value['input_title'] ?></a></h4>
                              <p class="mb-0"><?php echo previewBody($value['text_content'], 15) ?></p>
                              <a class="thm-btn mini-btn brd-rd3" href="/readblog?id=<?php echo $value['hash_id'] ?>" title="">Read More</a>
                          </div>
                      </div>
                    <?php else: ?>
                    <div class="post-style1 mb-30 overflow-hidden d-flex flex-wrap brd-rd10 bg-color19">
                      <div class="post-img1"><a href="/readblog?id=<?php echo $value['hash_id'] ?>" title=""><img class="img-fluid" src="<?php echo $value['image_1'] ?>" alt="Post Image 2"></a></div>
                      <div class="post-info1 bg-color20">
                        <!-- <span class="post-cat d-block"><a class="bg-color19" href="javascript:void(0);" title="">Branding</a></span> -->
                        <h4 class="mb-0"><a href="/readblog?id=<?php echo $value['hash_id'] ?>" title=""><?php echo $value['input_title'] ?></a></h4>
                        <p class="mb-0"><?php echo previewBody($value['text_content'], 15) ?></p>
                        <a class="thm-btn mini-btn brd-rd3" href="/readblog?id=<?php echo $value['hash_id'] ?>" title="">Read More</a>
                      </div>
                    </div>
                  <?php endif; ?>

                  <?php endforeach; ?>
              </div>
          </div>
      </div>
  </section>
<?php endif; ?>



<?php if ($testimonial): ?>
  <section style="background: <?php echo $testimonialSection['bgcolor_section'] ?>">
              <div class="w-100 blue-layer opc7 pt-100 pb-70 position-relative">
                  <div class="fixed-bg patern-bg bg-color25 back-blend-multiply"></div>
                  <div class="container">
                      <div class="testi-wrap2 w-100">
                          <div class="testi-caro">
                              <?php foreach ($testimonial as $key => $value): ?>
                                <div class="testi-item style2">
                                    <div class="testi-thumb position-relative d-inline-block">
                                        <img class="img-fluid rounded-circle" src="<?php echo $value['image_1'] ?>" alt="Testimonial Image 1">
                                    </div>
                                    <div class="testi-item-inner">
                                        <p class="mb-0" style="color: <?php echo $testimonialSection['textcolor_text'] ?>"><?php echo $value['text_testimony'] ?></p>
                                        <div class="testi-info">
                                            <!-- <img class="img-fluid" src="/public/assets/images/sign.png" alt="Sign"> -->
                                            <div class="testi-info-inner">
                                                <h6 class="mb-0"><?php echo $value['input_name'] ?></h6>
                                                <span class="d-block"><?php echo $value['input_organization'] ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              <?php endforeach; ?>
                          </div>
                      </div><!-- Testimonials Style 2 -->
                  </div>
              </div>
          </section>
<?php endif; ?>



    <!-- <section>
        <div class="w-100 pt-80 pb-80 position-relative">
            <div class="fixed-bg" style="background-image: url(/public/jsimages/newsletter.jpeg);"></div>
            <div class="container">
            <div class="newsletter-wrap w-100">
                    <div class="row">
                        <div class="col-md-9 col-sm-12 col-lg-8">
                        <div class="newsletter w-100 text-center">
                                <div class="newsletter-inner d-inline-block">
                                    <h2 class="mb-0">Sign Up To Our Newsletter</h2>
                                    <p class="mb-0">To get the latest news from us please subscribe your email.</p>
                                    <form method="post" class="rounded-pill overflow-hidden position-relative w-100 d-inline-block">
                                    <input type="email" name="email" placeholder="Subscribe to our Newsletter">
                                    <button type="submit" name="submitted"><i class="fas fa-paper-plane"></i></button>
                                    </form>
                                </div>
                        </div>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </section> -->




<?php include "includes/footer.php"; ?>
<script type="text/javascript">
let slideIndex = 0;
showSlides();

function showSlides() {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}
  slides[slideIndex-1].style.display = "block";
  setTimeout(showSlides, 8000); // Change image every 8 seconds
}






// let slideIndex = 1;
// showSlides(slideIndex);
//
// // Next/previous controls
// function plusSlides(n) {
//   showSlides(slideIndex += n);
// }
//
// // Thumbnail image controls
// function currentSlide(n) {
//   showSlides(slideIndex = n);
// }
//
// function showSlides(n) {
//   let i;
//   let slides = document.getElementsByClassName("mySlides");
//   let dots = document.getElementsByClassName("dot");
//   if (n > slides.length) {slideIndex = 1}
//   if (n < 1) {slideIndex = slides.length}
//   for (i = 0; i < slides.length; i++) {
//     slides[i].style.display = "none";
//   }
//   for (i = 0; i < dots.length; i++) {
//     dots[i].className = dots[i].className.replace(" active", "");
//   }
//   slides[slideIndex-1].style.display = "block";
//   dots[slideIndex-1].className += " active";
// }
</script>
