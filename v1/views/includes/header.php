<?php
$error= [];

if (isset($_POST['submitted'])) {
  $hashid = time().rand(10000, 99999);
  $new['date_created'] = date("Y-m-d");
  $new['time_created'] = date("H:i:s");
  $new['hash_id'] = $hashid;
  $new['input_email'] = $_POST['email'];
  $check = selectContent($conn,"read_newsletter",['input_email'=>$_POST['email']]);
  if(count($check) > 0){
    $_SESSION['success'] = "Already subscribed";
    header("Location:/home#newsletter");
    exit();
  }


  insertSafe($conn,"read_newsletter",$new);


require APP_PATH.'/phpm/PHPMailerAutoload.php';

// If necessary, modify the path in the require statement below to refer to the
// location of your Composer autoload.php file.
// require 'phpm/PHPMailerAutoload.php';


// Instantiate a new PHPMailer
$mail = new PHPMailer;

// Tell PHPMailer to use SMTP
$mail->isSMTP();

// Replace sender@example.com with your "From" address.
// This address must be verified with Amazon SES.
$mail->setFrom($site_email, 'OneInchDigital');
// $mail->AddReplyTo('info@cianamedia.com', 'Ciana Media');

// Replace recipient@example.com with a "To" address. If your account
// is still in the sandbox, this address must be verified.
// Also note that you can include several addAddress() lines to send
// email to multiple recipients.
$mail->addAddress($_POST['email']);

// Replace smtp_username with your Amazon SES SMTP user name.
$mail->Username = $site_email;

// Replace smtp_password with your Amazon SES SMTP password.
$mail->Password = getenv("EMAIL_PASSWORD");

// Specify a configuration set. If you do not want to use a configuration
// set, comment or remove the next line.
// $mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');

// If you're using Amazon SES in a region other than US West (Oregon),
// replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP
// endpoint in the appropriate region.
$mail->Host = 'smtp.gmail.com';

// The subject line of the email
$mail->Subject = 'Welcome to The OneInchDigital';

// The HTML-formatted body of the email
$mail->Body = "<h3>Welcome</h3>
<p>

<p>Thank you for subscribing to our newsletters.</p>

<p>If you require any further information, feel free to contact us: info@vibecitybooths.com</p>

<p>Welcome to The Justinches</p>
";

// Tells PHPMailer to use SMTP authentication
$mail->SMTPAuth = true;

// Enable TLS encryption over port 587
$mail->SMTPSecure = 'tls';
$mail->Port = 587;
// Tells PHPMailer to send HTML-formatted email
$mail->isHTML(true);

// The alternative email body; this is only displayed when a recipient
// opens the email in a non-HTML email client. The \r\n represents a
// line break.
$mail->AltBody = "Do not send a reply to this mail";

if(!$mail->send()) {
  // die(var_dump($mail->Body));
  $_SESSION['failed'] = 'Newsletter Subscription was not successful. Try again after some time.';
  header("Location:".$_SERVER['REQUEST_URI']);
   //  exit();
   // echo "Email not sent!, error occured" , PHP_EOL;
   exit();
     // die(var_dump("Email not sent. " , $mail->ErrorInfo , PHP_EOL));
} else {
   $_SESSION['success'] = 'Newsletter Subscription successful!';
   header("Location:".$_SERVER['REQUEST_URI']);
   //  exit();
   // echo "Email sent!" , PHP_EOL;
   exit();
}




  // $msg = "Newsletter Subscription Successful";
  // header("Location:/home?message=$msg");
  // die;

}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="" />
      <meta name="keywords" content="" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

      <link rel="icon" href="<?php echo $logo_directory ?>" sizes="35x35" type="image/png">
      <title><?php echo $site_name ?> | <?php echo $page_title ?></title>

      <link rel="stylesheet" href="/public/assets/css/all.min.css">
      <link rel="stylesheet" href="/public/assets/css/elegenticons.css">
      <link rel="stylesheet" href="/public/assets/css/animate.min.css">
      <link rel="stylesheet" href="/public/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="/public/assets/css/bootstrap-select.min.css">
      <link rel="stylesheet" href="/public/assets/css/owl.carousel.min.css">
      <link rel="stylesheet" href="/public/assets/css/jquery.fancybox.min.css">
      <link rel="stylesheet" href="/public/assets/css/perfect-scrollbar.css">
      <link rel="stylesheet" href="/public/assets/css/slick.css">
      <link rel="stylesheet" href="/public/assets/css/style.css">
      <link rel="stylesheet" href="/public/assets/css/responsive.css">
      <link rel="stylesheet" href="/public/assets/css/color.css">

      <!-- REVOLUTION STYLE SHEETS -->
      <link rel="stylesheet" href="/public/assets/css/revolution/settings.css">
      <!-- REVOLUTION LAYERS STYLES -->
      <link rel="stylesheet" href="/public/assets/css/revolution/layers.css">
      <!-- REVOLUTION NAVIGATION STYLES -->
      <link rel="stylesheet" href="/public/assets/css/revolution/navigation.css">
      <link rel="stylesheet" href="/public/assets/css/slidder.css">
</head>
<body>
  <script src="/ajax/ajax.js"></script>
      <main>
      <header class="stick style1 w-100 d-flex flex-wrap justify-content-between align-items-center">
            <div class="logo"><h1 class="mb-0"><a href="/" title="Home"><img class="img-fluid" style="width: 100px; height: 50px;" src="<?php echo $logo_directory ?>" alt="Logo" srcset="<?php echo $logo_directory ?>"></a></h1></div>
            <div class="menu-btns d-inline-flex">
                  <!-- <a class="search-btn rounded-circle" href="javascript:void(0);" title=""><i class="fas fa-search"></i></a> -->
                  <a class="menu-btn rounded-circle" href="javascript:void(0);" title=""><i class="fas fa-align-justify"></i></a>
            </div>

            <div class="menu-wrap">
            <span class="menu-cls-btn rounded-circle"><i class="icon_close"></i></span>
                  <ul class="mb-0 list-unstyled w-100" style="text-align:center;">
                      <li><a href="/" title="" style="font-size: 40px;">Home</a></li>
                      <li><a href="/about" title="" style="font-size: 40px;">About</a></li>
                      <li><a href="/services" title="" style="font-size: 40px;">Services</a></li>
                      <li><a href="/digital-innovations" title="" style="font-size: 40px;">Digital Innovations</a></li>
                      <li><a href="/contact" title="" style="font-size: 40px;">Contact</a></li>

            </div><!-- Menu Wrap -->
      </header><!-- Header -->
      <div class="search-wrap w-100 d-flex flex-wrap align-items-center justify-content-center">
            <span class="search-cls-btn rounded-circle"><i class="icon_close"></i></span>
            <form class="w-100">
                  <input type="text" placeholder="Search here....">
                  <button type="submit"><i class="fas fa-search"></i></button>
            </form>
      </div><!-- Search Wrap -->
      <div class="sticky-header w-100 position-fixed">
            <div class="logo"><h1 class="mb-0"><a href="/" title="Home"><img class="img-fluid" style="width: 100px; height: 50px;" src="<?php echo $logo_directory2 ?>" alt="Logo" srcset="<?php echo $logo_directory2 ?>"></a></h1></div>
            <nav class="menu-wrap2">
                  <ul class="mb-0 list-unstyled w-100">
                  <li class="menu-item-has-children"><a href="/" title="">Home</a>
                  </li>
                  <li class="menu-item-has-children"><a href="/about" title="">About</a>
                  <li class="menu-item-has-children"><a href="/services" title="">Services</a>
                  <li class="menu-item-has-children"><a href="/digital-innovations" title="">Digital Innovations</a>
                  <li><a href="/contact" title="">Contact</a></li>
                  <!-- <li><a href="/getaquote" class="getaquote">Get a quote</a></li> -->
                  </ul>
            </nav>
      </div><!-- Sticky Header -->
