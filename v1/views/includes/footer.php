
        <!-- <footer>
                <div class="w-100 drk-bg4 pt-100 pb-70">
                    <div class="container">
                        <div class="footer-wrap text-center w-100">
                            <div class="footer-inner d-inline-block w-100">
                                <div class="logo d-inline-block">
                                    <a href="/" title=""><img class="img-fluid" style="width: 100px; height: 50px;" src="<?php echo $logo_directory ?>" alt="Home" srcset="<?php echo $logo_directory ?>"></a>
                                </div>
                                <p class="mb-5"><?php echo $site_description ?></p>
                                <div class="scl d-inline-block text-white">
                                    <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-twitter"></i></a>
                                    <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                    <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-behance"></i></a>
                                    <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-pinterest-p"></i></a>
                                    <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><
                    </div>
                </div>
            </footer> -->
            <footer>
                <div class="w-100 drk-bg4 pt-90 pb-40">
                    <div class="container">
                        <div class="footer-inner2 w-100">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="widget w-100 mb-55">
                                        <div class="logo d-inline-block">
                                        <a href="/" title=""><img class="img-fluid" style="width: 100px; height: 50px;" src="<?php echo $logo_directory ?>" alt="Logo" srcset="<?php echo $logo_directory ?>"></a>
                                        </div>
                                        <p class="mb-0"><?php echo $site_description ?></p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="widget w-100 mb-55 text-center">
                                        <h3>Contact Us (Nigeria)<span class="thm-clr">.</span></h3>
                                        <ul class="cont-info-list mb-0 list-unstyled w-100">
                                            <li class="w-100"><span>Address:</span> <?php echo $site_address ?></li>
                                            <li class="w-100"><span>Phone:</span><?php echo $site_phone ?></li>
                                            <li class="w-100"><span>Email:</span> <a href="mailto:<?php echo $site_email ?>" title=""><?php echo $site_email ?></a></li>
                                        </ul>
                                        <div class="scl4 w-100">
                                            <a class="d-inline-block rounded-circle facebook" href="<?php echo $fbLink ?>" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                            <a class="d-inline-block rounded-circle twitter" href="<?php echo $twitterLink ?>" title="Twitter" target="_blank"><i class="fab fa-twitter"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="widget w-100 mb-55 text-center">
                                        <h3>Contact Us (US)<span class="thm-clr">.</span></h3>
                                        <ul class="cont-info-list mb-0 list-unstyled w-100">
                                            <li class="w-100"><span>Address:</span> <?php echo $site_address2 ?></li>
                                            <li class="w-100"><span>Phone:</span><?php echo $site_phone2 ?></li>
                                            <li class="w-100"><span>Email:</span> <a href="mailto:<?php echo $site_email2 ?>" title=""><?php echo $site_email2 ?></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <!-- <div class="col-md-3">

                                </div> -->
                            </div>
                        </div>

                    </div>
                </div>
            </footer>

            <div class="bootom-bar drk-bg2 text-center w-100">
                  <p class="mb-0">&copy; All right reserved <script>document.write(new Date().getFullYear())</script>. <a href="/" title="Home"> <?php echo $site_name; ?> </a> - Creative Agency.</p>
            </div>




<!--
{{!--
      <div class="subscribe-popup-wrap flex-wrap align-items-center justify-content-center w-100 position-fixed h-100">
            <div class="subscribe-popup-inner text-center d-inline-block overflow-hidden brd-rd20">
                  <div class="subscribe-popup-iner w-100 brd-rd20 overflow-hidden" style="background-image: url(assets/images/popup-top-bg.png);">
                  <span class="popup-cls-btn rounded-circle"><i class="icon_close"></i></span>
                  <div class="popup-title w-100">
                        <h2 class="mb-0">Subscribe get notified!</h2>
                        <p class="mb-0">Garcia, a versatil web designer. Phasellus vehicula the justo eg diam in posuere phasellus eget sem</p>
                  </div>
                  <form class="w-100 position-relative">
                        <input type="email" placeholder="Subscribe your email">
                        <button type="submit"><i class="icon_mail_alt"></i></button>
                  </form>
                  <i>Service</i>
                  </div>
                  <div class="subscribe-popup-bottom text-left w-100" style="background-image: url(assets/images/popup-bottom-bg.jpg);">
                  <div class="row align-items-center">
                        <div class="col-md-6 col-sm-6 col-lg-6">
                              <div class="scl2 d-inline-block">
                              <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-facebook-f"></i></a>
                              <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-twitter"></i></a>
                              <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                              <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-behance"></i></a>
                              <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-pinterest-p"></i></a>
                              <a href="javascript:void(0);" title="" target="_blank"><i class="fab fa-instagram"></i></a>
                              </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                              <span class="emailinfo w-100 d-block">
                              <i class="icon_mail_alt"></i>
                              <a href="javascript:void(0);" title="">info@email.com</a>
                              <span class="d-block">online support</span>
                              </span>
                        </div>
                  </div>
                  </div>
            </div>
      </div> --}} -->

      <!-- Subscribe Popup Wrap -->


      </main><!-- Main Wrapper -->


<script src="/public/assets/js/jquery.min.js"></script>
<script src="/public/assets/js/popper.min.js"></script>
<script src="/public/assets/js/bootstrap.min.js"></script>
<script src="/public/assets/js/bootstrap-select.min.js"></script>
<script src="/public/assets/js/wow.min.js"></script>
<script src="/public/assets/js/owl.carousel.min.js"></script>
<script src="/public/assets/js/counterup.min.js"></script>
<script src="/public/assets/js/jquery.fancybox.min.js"></script>
<script src="/public/assets/js/perfect-scrollbar.min.js"></script>
<script src="/public/assets/js/slick.min.js"></script>
<script src="/public/assets/js/particles.min.js"></script>
<script src="/public/assets/js/particle-int.js"></script>
<script src="/public/assets/js/custom-scripts.js"></script>

<script src="/public/assets/js/revolution/jquery.themepunch.tools.min.js"></script>
<script src="/public/assets/js/revolution/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="/public/assets/js/revolution/extensions/revolution.extension.actions.min.js"></script>
<script src="/public/assets/js/revolution/extensions/revolution.extension.carousel.min.js"></script>
<script src="/public/assets/js/revolution/extensions/revolution.extension.kenburn.min.js"></script>
<script src="/public/assets/js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/public/assets/js/revolution/extensions/revolution.extension.migration.min.js"></script>
<script src="/public/assets/js/revolution/extensions/revolution.extension.navigation.min.js"></script>
<script src="/public/assets/js/revolution/extensions/revolution.extension.parallax.min.js"></script>
<script src="/public/assets/js/revolution/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/public/assets/js/revolution/extensions/revolution.extension.video.min.js"></script>
<script src="/public/assets/js/revolution/revolution-init.js"></script>

<!-- <script src="/public/assets/js/slidder-script.js"></script> -->
<!-- Start of HubSpot Embed Code -->
  <!-- <script type="text/javascript" id="hs-script-loader" async defer src="//js-na1.hs-scripts.com/22781777.js"></script> -->
<!-- End of HubSpot Embed Code -->

    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
      var swiper = new Swiper(".mySwiper", {
        slidesPerView: 3,
        spaceBetween: 0,
        slidesPerGroup: 3,
        loop: false,
        loopFillGroupWithBlank: false,
        autoplay: {
          delay: 5000,
          disableOnInteraction: false,
        },

        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        breakpoints: {
            280: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
            320: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
            480: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        640: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
          768: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          820: {
            slidesPerView: 2,
            spaceBetween: 10,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 20,
          },

        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
      });
    </script>


<script>
        let dSwitch = document.querySelectorAll("#dSwitch");
        let bs = document.querySelectorAll(".bs");


        dSwitch.forEach((a, index) => {
            a.style.cursor = "pointer";
            bs.forEach((bt, i) => bt.style.display = "none");

            bs[0].style.display = "initial";

            a.addEventListener("click", () => {

            bs.forEach((bt, i) =>  bt.style.display = "none");

            for(i = 0; i< bs.length; i++) {
                if(i === index) bs[i].style.display = "initial";
            }

        });

    });

</script>
<?php
$chatPlugin = selectContent($conn, "settings_chat_plugin", ['visibility' => 'show'])[0];
 ?>
 <?= $chatPlugin['input_chat_script'] ?>
<!--Start of Tawk.to Script-->

<!--End of Tawk.to Script-->



</body>
</html>
