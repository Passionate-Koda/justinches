<?php
if (isset($_GET['id'])) {
  $where['hash_id'] = $_GET['id'];
  $blogdetails = selectContent($conn, "panel_blog", $where);

  if (count($blogdetails) < 1) {
    header("location:/home");
    exit();
  }

}else{
header("location:/home");
exit();
}


$page_title = $blogdetails[0]['input_title'];
 ?>
 <?php include "includes/header.php"; ?>

<section>
      <div class="w-100 dark-layer3 opc85 position-relative">
      <!-- "./../../../public/jsimages/about_us_page_main_image.jpg" -->
            <div class="fixed-bg" style="background-image: url(<?php echo $blogdetails[0]['image_1'] ?>);"></div>
            <div class="container">
            <div class="page-title text-center w-100">
                  <h1 class="mb-0"><?php echo $blogdetails[0]['input_title'] ?><span class="thm-clr">.</span></h1>
            </div>
            <!-- Page Title -->
            <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/" title=""><i class="icon_house"></i></a></li>
                  <li class="breadcrumb-item active"><?php echo $blogdetails[0]['input_title'] ?></li>
            </ol>
            <!-- Breadcrumb -->
            </div>
      </div>
</section>




<section>
<div class="w-100 pt-155 pb-120 position-relative">
    <div class="container">
        <div class="blog-detail-wrap w-100">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="leftside">
                        <div class="theiaStickySidebar">
                            <div class="blog-detail w-100">
                                <div class="blog-img-info w-100">
                                    <img class="img-fluid w-100" src="<?php echo $blogdetails[0]['image_1'] ?>" alt="Blog Detail Image">
                                    <h2 class="mb-0"><?php echo $blogdetails[0]['input_title'] ?></h2>
                                    <ul class="pst-dtl-mta mb-0 list-unstyled d-flex flex-wrap">
                                        <li><strong><?php echo decodeDate($blogdetails[0]['date_created']) ?></strong></li>
                                        <!-- <li>Posted by <a href="javascript:void(0);" title="">David Richards</a></li>
                                        <li>In: <a href="javascript:void(0);" title="">Graphic Design</a></li>
                                        <li>Comments: <span>0</span></li> -->
                                    </ul>
                                </div>
                                <?php echo $blogdetails[0]['text_content'] ?>
                            </div>


                        </div>
                    </div>
                </div>




            </div>
        </div><!-- Blog Detail Wrap -->
    </div>
</div>
</section>



<?php include "includes/footer.php"; ?>
