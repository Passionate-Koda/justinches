<?php
$page_title = "Services";

$service = selectContent($conn, "panel_service", ['visibility' => "show"]);
$preamble = selectContent($conn, "settings_service_preamble", ['visibility' => "show"])[0];
$parallax = selectContent($conn, "settings_service_parallax_image", ['visibility' => "show"])[0];
$casestudy = selectContent($conn, "panel_case_study", ['visibility' => "show"]);
$casestudySection = selectContent($conn, "settings_service_case_study_section", ['visibility' => "show"])[0];
$breadcrumb = selectContent($conn, "settings_service_breadcrumb", ['visibility' => "show"])[0];
$bestserviceSection = selectContent($conn, "settings_service_best_service_section", ['visibility' => "show"])[0];
 ?>

 <?php include "includes/header.php"; ?>

<section>
      <div class="w-100 dark-layer3 opc85 position-relative">
      <!-- "./../../../public/jsimages/about_us_page_main_image.jpg" -->
            <div class="fixed-bg" style="background-image: url(<?php echo $breadcrumb['image_1'] ?>);"></div>
            <div class="container">
            <div class="page-title text-center w-100">
                  <h1 class="mb-0"><?php echo $breadcrumb['input_title'] ?><span class="thm-clr">.</span></h1>
            </div>
            <!-- Page Title -->
            <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/" title=""><i class="icon_house"></i></a></li>
                  <li class="breadcrumb-item active">Services</li>
            </ol>
            <!-- Breadcrumb -->
            </div>
      </div>
</section>

<section style="background: <?php echo $preamble['bgcolor_section'] ?>">
          <div class="w-100 pb-70 pt-70 position-relative">
              <div class="container">
                  <div class="w-100 pt-5">
                      <div class="row align-items-center">
                          <div class="col-md-12">
                              <div class=" w-100">
                                  <p class="mb-0"><?php echo $preamble['text_content'] ?></p>
                              </div>
                          </div>
                      </div>
                  </div><!-- About Style 1 -->
              </div>
              </div>

</section>
<!-- <section class="mb-5"> </section> -->

<section style="background: <?php echo $bestserviceSection['bgcolor_section'] ?>">
<div class="w-100 pt-70 pb-120">
<div class="container">
    <div class="sec-title style2 text-center w-100 mb-55">
        <div class="sec-title-inner d-inline-block">
            <h2 class="mb-0" style="color: <?php echo $bestserviceSection['textcolor_title'] ?>"><?php echo $bestserviceSection['input_title'] ?></h2>
            <span class="d-inline-block" style="color: <?php echo $bestserviceSection['textcolor_subtitle'] ?>"><?php echo $bestserviceSection['input_sub_title'] ?></span>
        </div>
    </div><!-- Sec Title -->
    <div class="serv-wrap2 style2 w-100 text-center">
        <div class="row">
            <?php foreach ($service as $key => $value): ?>
              <div class="col-md-6 col-sm-6 col-lg-4">
                  <div class="srv-box2 style clr1 w-100">
                      <i class="rounded-circle <?php echo $value['icon_icon'] ?>" style="color: <?php echo $value['textcolor_icon'] ?>; background: <?php echo $value['bgcolor_icon'] ?>"></i>
                      <div class="srv-info2 w-100">
                          <h3 class="mb-0" style="color: <?php echo $value['textcolor_title'] ?>"><?php echo $value['input_title'] ?></h3>
                          <p class="mb-2" style="color: <?php echo $value['textcolor_text'] ?>"><?php echo $value['input_short_description'] ?></p>
                          <a href="/view-service?id=<?php echo $value['hash_id'] ?>" class="btn text-white"  style="background: <?php echo $bestserviceSection['bgcolor_readmore_button'] ?>">Read More</a>
                      </div>
                  </div>
              </div>
            <?php endforeach; ?>
        </div>
    </div><!-- Services Style 2 -->
</div>
</div>
</section>



<section>
    <div class="w-100 pb-155 pt-180 black-layer opc65 position-relative">
        <div class="fixed-bg" style="background-attachment: fixed;background-image: url(<?php echo $parallax['image_1'] ?>);"></div>
        <div class="container">
        <div class="video-pres-wrap text-center w-100">
                <div class="video-pres-inner d-inline-block">
                  <h4 style="position:relative; color: #fff;"><?php echo $parallax['input_text'] ?></h4>
                </div>
        </div>
        </div>
    </div>
</section>
<!-- <section class="mb-5"> </section> -->
<section style="background: <?php echo $casestudySection['bgcolor_section'] ?>">
<div class="w-100 pt-70 pb-120">
<div class="container">
    <div class="sec-title style2 text-center w-100 mb-55">
        <div class="sec-title-inner d-inline-block">
            <h2 class="mb-0" style="color: <?php echo $casestudySection['textcolor_title'] ?>"><?php echo $casestudySection['input_title'] ?></h2>
            <span class="d-inline-block" style="color: <?php echo $casestudySection['textcolor_subtitle'] ?>"><?php echo $casestudySection['input_sub_title'] ?></span>
        </div>
    </div><!-- Sec Title -->
    <div class="serv-wrap2 style2 w-100 text-center">
        <div class="row">
            <?php foreach ($casestudy as $key => $value): ?>
              <div class="col-md-6 col-sm-6 col-lg-4">
                  <div class="srv-box2 style clr1 w-100">
                    <!-- <div class=""> -->
                      <img src="<?php echo $value['image_1'] ?>" alt="" style="vertical-align: middle;width: 150px;height: 150px;border-radius: 50%;">
                    <!-- </div> -->
                      <div class="srv-info2 w-100">
                        <h3 class="mb-0"><?php echo $value['input_client_name'] ?></h3><br>
                        <a href="/case-study?id=<?php echo $value['hash_id'] ?>" class="btn btn-danger"><p class="mb-0 text-white">See Case Study</p></a>  <br><br>
                      </div>
                  </div>
              </div>
            <?php endforeach; ?>
        </div>
    </div><!-- Services Style 2 -->
</div>
</div>
</section>

<?php include "includes/footer.php"; ?>
