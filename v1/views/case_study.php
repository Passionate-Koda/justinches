<?php
if (isset($_GET['id'])) {
  $where['hash_id'] = $_GET['id'];
  $casedetails = selectContent($conn, "panel_case_study", $where);

  if (count($casedetails) < 1) {
    header("location:/home");
    exit();
  }

}else{
header("location:/home");
exit();
}


$page_title = "Case Study | ".$casedetails[0]['input_client_name'];
 ?>
 <?php include "includes/header.php"; ?>

<section>
      <div class="w-100 dark-layer3 opc85 position-relative">
      <!-- "./../../../public/jsimages/about_us_page_main_image.jpg" -->
            <div class="fixed-bg" style="background-image: url(<?php echo $casedetails[0]['image_1'] ?>);"></div>
            <div class="container">
            <div class="page-title text-center w-100">
                  <h1 class="mb-0"><?php echo $casedetails[0]['input_client_name'] ?><span class="thm-clr">.</span></h1>
            </div>
            <!-- Page Title -->
            <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/" title=""><i class="icon_house"></i></a></li>
                  <li class="breadcrumb-item active"><?php echo $casedetails[0]['input_client_name'] ?></li>
            </ol>
            <!-- Breadcrumb -->
            </div>
      </div>
</section>




<section>
<div class="w-100 pt-155 pb-120 position-relative">
    <div class="container">
        <div class="blog-detail-wrap w-100">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="leftside">
                        <div class="theiaStickySidebar">
                            <div class="blog-detail w-100">
                                <div class="blog-img-info w-100">
                                    <!-- <img class="img-fluid w-100" src="<?php echo $casedetails[0]['image_1'] ?>" alt="Blog Detail Image"> -->
                                    <!-- <h2 class="mb-0"><?php echo $casedetails[0]['input_client_name'] ?></h2><br> -->
                                </div>
                                <?php echo $casedetails[0]['text_description'] ?>
                                <br><br><br>

                                <a class="thm-btn mini-btn brd-rd3" href="/contact" title="">Contact Us</a>
                            </div>


                        </div>
                    </div>
                </div>




            </div>
        </div><!-- Blog Detail Wrap -->
    </div>
</div>
</section>



<?php include "includes/footer.php"; ?>
