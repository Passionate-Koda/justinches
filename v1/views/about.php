<?php
$page_title = "About Us";

$unique_services = selectContent($conn, "panel_about_unique_services", ['visibility' => "show"]);
$aboutDetails = selectContent($conn, "panel_about", ['visibility' => "show"]);
$team = selectContent($conn, "panel_team", ['visibility' => "show"]);
$service = selectContent($conn, "panel_service", ['visibility' => "show"]);
$partners = selectContent($conn, "panel_partners", ['visibility' => "show"]);
$corevalues = selectContent($conn, "panel_core_values", ['visibility' => "show"]);
$parallax = selectContent($conn, "settings_about_parallax_image", ['visibility' => "show"])[0];
$images = selectContent($conn, "images", ['asset_hash_id' => $aboutDetails[0]['hash_id']]);
$breadcrumb = selectContent($conn, "settings_about_breadcrumb", ['visibility' => "show"])[0];
$corevalueSection = selectContent($conn, "settings_about_core_values_section", ['visibility' => "show"])[0];
$teamSection = selectContent($conn, "settings_about_team_section", ['visibility' => "show"])[0];
$partnersSection = selectContent($conn, "settings_about_partners_section", ['visibility' => "show"])[0];
$preambleSection = selectContent($conn, "settings_about_preamble_section", ['visibility' => "show"])[0];
$collaborativeSection = selectContent($conn, "settings_about_collaborative_section", ['visibility' => "show"])[0];
 ?>
<?php include "includes/header.php"; ?>
<section>
      <div class="w-100 dark-layer3 opc85 position-relative">
      <!-- "./../../../public/jsimages/about_us_page_main_image.jpg" -->
            <div class="fixed-bg" style="background-image: url(<?php echo $breadcrumb['image_1'] ?>);"></div>
            <div class="container">
            <div class="page-title text-center w-100">
                  <h1 class="mb-0"><?php echo $breadcrumb['input_title'] ?><span class="thm-clr">.</span></h1>
            </div>
            <!-- Page Title -->
            <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#" title=""><i class="icon_house"></i></a></li>
                  <li class="breadcrumb-item active"><?php echo $breadcrumb['input_title'] ?></li>
            </ol>
            <!-- Breadcrumb -->
            </div>
      </div>
</section>

<?php if($aboutDetails): ?>

<section style="background: <?php echo $preambleSection['bgcolor_section'] ?>">
      <div class="w-100 pt-155 pb-70 position-relative">
            <div class="container">
            <div class="about-wrap6 w-100">
                  <h2 class="mb-0" style="color: <?php echo $preambleSection['textcolor_title'] ?>"><?php echo $aboutDetails[0]['input_title']; ?></h2>

                  <div class="mb-0" style="color: <?php echo $preambleSection['textcolor_text'] ?>"><?php echo $aboutDetails[0]['text_content']; ?>

                  </div>
                  <div class="about-img w-100">
                        <div class="row">
                        <?php foreach ($images as $key => $value): ?>
                          <div class="col-md-6 col-sm-6 col-lg-6">
                                <img class="img-fluid mt-35 w-100" src="<?php echo $value['image_1']; ?>" alt="About Image 1">
                                <!-- <img class="img-fluid mt-35 w-100" src="{{this.aboutInfoImg1}}" alt="About Image 1"> -->
                          </div>
                        <?php endforeach; ?>
                        </div>
                  </div>
                  <div class="about-info6 w-100">
                        <div class="row">
                        <div class="col-md-6 col-sm-12 col-lg-6">

                              <ul class="about-list mt-30 mb-0 list-unstyled w-100">
                                    <?php $lists2 = explode(",",  $aboutDetails[0]['text_arr']); ?>
                                    <?php foreach($lists2 as $list2): ?>
                                          <li style="color: <?php echo $preambleSection['textcolor_text'] ?>"><a href="javascript:void(0);" title=""><?php echo $list2; ?></a></li>
                                    <?php endforeach; ?>
                              </ul>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-6">
                              <p class="mb-0 mt-40" style="color: <?php echo $preambleSection['textcolor_text'] ?>"> <?php echo $aboutDetails[0]['text_description']; ?> </p>
                        </div>
                        </div>
                  </div>
            </div><!-- About Style 6 -->
            </div>
      </div>
</section>
<?php endif; ?>
<section style="background: <?php echo $collaborativeSection['bgcolor_section'] ?>">
<div class="w-100 pt-70">
    <div class="container">
      <div class="w-100">
          <div class="sec-title text-center w-100 mb-55">
              <div class="sec-title-inner d-inline-block">
                  <h2 class="mb-0" style="color: <?php echo $collaborativeSection['textcolor_title'] ?>"><?php echo $collaborativeSection['input_title'] ?></h2>
                  <span class="d-inline-block thm-clr" style="color: <?php echo $collaborativeSection['textcolor_subtitle'] ?>"><?php echo $collaborativeSection['input_sub_title'] ?></span>
              </div>
          </div>
      </div>
        <div class=" swiper mySwiper  text-center">
            <div class="swiper-wrapper ">
              <?php foreach ($unique_services as $key => $value): ?>
                <div class="swiper-slide col-md-4 col-sm-6 col-lg-4" id="dSwitch">
                    <div class="srv-box1 mb-30 w-100 brd-rd10" style="height:230px">
                      <h1 class="<?php echo $value['icon_icon'] ?>"></h1>
                        <!-- <img class="img-fluid" src="/public/jsimages/61f6fc0247cbb.png" alt="Service Icon 1"> -->
                        <div class="srv-info1 mt-30 w-100">
                            <h3 class="mb-0" style="color: <?php echo $collaborativeSection['textcolor_title'] ?>"><?php echo $value['input_title'] ?></h3>
                            <!-- <p class="mb-0 mt-20">Place lights there don't grass, may dry from. Us he, itself may saw beginning. Night upon, they're created.</p> -->
                        </div>
                    </div>
                </div>
              <?php endforeach; ?>
            </div>
    </div>
  </div>
  <!-- <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div> -->
  <!-- <div class="swiper-pagination"></div> -->
</div>
</section>

<section style="background: <?php echo $collaborativeSection['bgcolor_section'] ?>">
        <?php foreach ($unique_services as $key => $value): ?>
          <div class="w-100 pb-70 pt-70 position-relative bs" id="bs">
              <div id="particles1" class="particles-js top-left" data-saturation="300" data-size="40" data-count="20" data-speed="2" data-hide="770" data-image="assets"></div>
              <div class="container">
                  <div class="about-wrap w-100">
                      <div class="row align-items-center">
                          <div class="col-md-12 col-sm-12 col-lg-7">
                              <div class="about-mckp slide-animation w-100">
                                  <img class="img-fluid" src="<?php echo $value['image_1'] ?>" alt="About Mockup 1">
                              </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-lg-5">
                              <div class="about-info w-100">
                                  <h2 class="mb-0" style="color: <?php echo $collaborativeSection['textcolor_title'] ?>"><?php echo $value['input_title'] ?></h2>
                                  <p class="mb-0" style="color: <?php echo $collaborativeSection['textcolor_text'] ?>"><?php echo $value['text_content'] ?></p>
                                  <!-- <a class="thm-btn mini-btn brd-rd3" href="/about" title="">ABOUT US</a> -->
                              </div>
                          </div>
                      </div>
                  </div><!-- About Style 1 -->
              </div>
              </div>
        <?php endforeach; ?>

</section>

<section>
    <div class="w-100 pb-155 pt-180 black-layer opc65 position-relative">
        <div class="fixed-bg" style="background-attachment: fixed;background-image: url(<?php echo $parallax['image_1'] ?>);"></div>
        <div class="container">
        <div class="video-pres-wrap text-center w-100">
                <div class="video-pres-inner d-inline-block">
                  <h4 style="position:relative; color: <?php echo $parallax['textcolor_text'] ?>"><?php echo $parallax['input_text'] ?></h4>
                </div>
        </div>
        </div>
    </div>
</section>

<?php if($team): ?>
<section style="background: <?php echo $teamSection['bgcolor_section'] ?>">
      <div class="w-100 pt-70 position-relative">
            <div class="sec-title style2 text-center w-100 mb-55">
            <div class="sec-title-inner d-inline-block">
                  <h2 class="mb-0" style="color: <?php echo $teamSection['textcolor_title'] ?>"><?php echo $teamSection['input_title'] ?></h2>
                  <span class="d-inline-block" style="color: <?php echo $teamSection['textcolor_subtitle'] ?>"><?php echo $teamSection['input_sub_title'] ?></span>
            </div>
            </div><!-- Sec Title -->
            <div class="team-wrap4 w-100">
            <div class="team-caro3">
                  <?php foreach ($team as $key => $value): ?>
                    <div class="team-box4 d-block overflow-hidden position-relative text-center">
                          <!-- <img class="img-fluid w-100" src="./../../../public/jsimages/team-img4-2.jpeg" alt="Team Image 1"> -->
                          <img class="img-fluid w-100" src="<?php echo $value['image_1']; ?>" alt="Team Image 1">
                          <div class="team-info4 position-absolute">
                          <h3 class="mb-0"><a href="javascript:void(0);"><?php echo $value['input_name']; ?></a></h3>
                          <span class="d-block text_uppercase"><?php echo $value['input_position']; ?></span>
                          </div>
                    </div>
                  <?php endforeach; ?>
            </div>
            </div><!-- Team Style 4 -->
      </div>
</section>
<?php endif; ?>

<?php if($corevalues): ?>
<section style="background: <?php echo $corevalueSection['bgcolor_section'] ?>">
      <div class="w-100 pt-120 pb-120">
            <div class="container">
            <div class="sec-title style2 text-center w-100 mb-55">
                  <div class="sec-title-inner d-inline-block">
                        <h2 class="mb-0" style="color: <?php echo $corevalueSection['textcolor_section_title'] ?>"><?php echo $corevalueSection['input_title'] ?></h2>
                        <span class="d-inline-block" style="color: <?php echo $corevalueSection['textcolor_subtitle'] ?>"><?php echo $corevalueSection['input_sub_title'] ?></span>
                  </div>
            </div><!-- Sec Title -->
            <div class="serv-wrap2 style2 w-100 text-center">
                  <div class="row">
                        <?php foreach ($corevalues as $key => $value): ?>
                          <div class="col-md-6 col-sm-6 col-lg-4">
                          <div class="srv-box2 style clr1 w-100">
                                <i class="rounded-circle <?php echo $value['icon_icon'] ?>" style="color: <?php echo $value['textcolor_icon'] ?>; background: <?php echo $value['bgcolor_icon'] ?>"></i>
                                <div class="srv-info2 w-100">
                                <h3 class="mb-2" style="color: <?php echo $corevalueSection['textcolor_title'] ?>"><?php echo $value['input_title']; ?></h3>
                                <p class="mb-2" style="color: <?php echo $corevalueSection['textcolor_text'] ?>"><?php echo $value['text_description']; ?></p>
                                </div>
                          </div>
                          </div>
                        <?php endforeach; ?>
                  </div>
            </div><!-- Services Style 2 -->
            </div>
      </div>
</section>


<?php endif; ?>
<?php if ($partners): ?>

        <section style="background: <?php echo $partnersSection['bgcolor_section'] ?>">
                <div class="w-100 pb-130 pt-70">
                    <!-- <div class="container"> -->
                    <div class="sec-title style2 text-center w-100 mb-55">
                          <div class="sec-title-inner d-inline-block">
                                <h2 class="mb-0" style="color: <?php echo $corevalueSection['textcolor_title'] ?>"><?php echo $partnersSection['input_title'] ?></h2>
                                <span class="d-inline-block" style="color: <?php echo $corevalueSection['textcolor_subtitle'] ?>"><?php echo $partnersSection['input_sub_title'] ?></span>
                          </div>
                    </div>
                        <div class="sponsor-wrap text-center w-100">
                            <div class="row align-items-center">
                                <?php foreach ($partners as $key => $value): ?>
                                  <div class="col-md-2">
                                      <div class="sponsor-item mt-30 w-100">
                                          <a href="javascript:void(0);" title=""><img class="img-fluid" src="<?php echo $value['image_1'] ?>" alt="Sponsor 1" style="width: 50%; height: 50%"></a>
                                      </div>
                                  </div>
                                <?php endforeach; ?>
                            </div>
                        </div><!-- Sponsor Style 1 -->
                    <!-- </div> -->
                </div>
            </section>
<?php endif; ?>

<?php include "includes/footer.php"; ?>
