-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 04, 2022 at 06:15 PM
-- Server version: 5.7.35-0ubuntu0.18.04.2
-- PHP Version: 7.2.34-24+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mckodevc_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `hash_id` varchar(255) NOT NULL,
  `portfolio` text,
  `bio` text,
  `phone_number` varchar(255) DEFAULT NULL,
  `facebook_link` varchar(255) DEFAULT NULL,
  `twitter_link` varchar(255) DEFAULT NULL,
  `linkedin_link` varchar(255) DEFAULT NULL,
  `instagram_link` varchar(225) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_1` varchar(255) DEFAULT NULL,
  `image_2` varchar(255) DEFAULT NULL,
  `image_3` varchar(255) DEFAULT NULL,
  `time_created` time NOT NULL,
  `date_created` date NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `login_status` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `verification` varchar(255) DEFAULT NULL,
  `profile_status` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL,
  `defaulted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `firstname`, `lastname`, `email`, `hash`, `hash_id`, `portfolio`, `bio`, `phone_number`, `facebook_link`, `twitter_link`, `linkedin_link`, `instagram_link`, `location`, `image_1`, `image_2`, `image_3`, `time_created`, `date_created`, `last_login`, `last_logout`, `login_status`, `level`, `verification`, `profile_status`, `user_status`, `defaulted`) VALUES
(7, 'Banji', 'Akole', 'banjimayowa@gmail.com', '$2y$10$nfIX.S/vu469XEOOr4nrjupfWxF2tHfUwpX7S0sH1eyaIY8tZivs.', 'j90819542aBn72i', '555666777888999000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '14:25:12', '2018-02-28', '2020-01-30 17:33:23', '2018-05-17 16:23:50', 'Logged In', 'MASTER', '1', NULL, '1', NULL),
(35, 'Tolu', 'Akintayo', 'tolubama@gmail.com', '$2y$10$.hRad97CI1P9nVgXQYuspuAfxgwmtSnyIOJhlp.gbhxFraAULFQyW', '161366667456o6l34u594t3', '555666777888999000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '16:44:34', '2021-02-18', NULL, NULL, NULL, 'MASTER', '1', NULL, '1', NULL),
(36, 'Odunola', 'Olabintan', 'olabintanodunola@yahoo.com', '$2y$10$hAAf9JbMF6YGYh3L.FrP3OBeAy6z2K/xTOTvaG8sdiGgmOoHMFAxy', '1613835852d1o9a7uo076n8729l', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15:44:12', '2021-02-20', NULL, NULL, NULL, 'MASTER', '1', NULL, '1', NULL),
(38, 'Mobolaji', 'Olatunbosun', 'bolajiolatubosun@gmail.com', '$2y$10$ux.R9Ezzlw41fC7svUdoNeYRyoFuCh8v6SpNbHgWgu/taUxTfE7Dy', '16163277030i1o6o7b92l3041maj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11:55:03', '2021-03-21', NULL, NULL, NULL, 'MASTER', '1', NULL, '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_auth`
--

CREATE TABLE `admin_auth` (
  `id` int(10) UNSIGNED NOT NULL,
  `auth` varchar(225) DEFAULT NULL,
  `created_by` varchar(225) DEFAULT NULL,
  `used_by` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_auth`
--

INSERT INTO `admin_auth` (`id`, `auth`, `created_by`, `used_by`, `date_created`, `time_created`) VALUES
(39, '191138', '1565905740k783la6e402o033', NULL, '2020-01-08', '10:33:09'),
(40, '446358', '1565905740k783la6e402o033', NULL, '2020-01-08', '17:32:19'),
(41, '986659', 'j90819542aBn72i', '15820373186h5mie15l1a6c3274', '2020-02-07', '15:17:28'),
(42, '729912', 'j90819542aBn72i', '1582040299142aoai8478bym978', '2020-02-18', '13:59:17'),
(43, '969892', 'j90819542aBn72i', NULL, '2020-03-04', '14:03:54'),
(44, '282867', 'j90819542aBn72i', '15849548497a607ll1a9o0ew477', '2020-03-13', '19:07:46'),
(45, '146070', 'j90819542aBn72i', NULL, '2020-03-23', '15:31:40'),
(46, '288139', 'j90819542aBn72i', '1586796471sscmo114635o2675', '2020-04-13', '15:56:10'),
(47, '815570', 'j90819542aBn72i', NULL, '2020-05-24', '16:06:40'),
(48, '576041', '1586796471sscmo114635o2675', '15904701667t55no15is25154', '2020-05-26', '02:07:36'),
(49, '947680', 'j90819542aBn72i', '15910148449t27ia6ti42358oy8l', '2020-05-30', '21:05:51'),
(50, '528954', 'j90819542aBn72i', '161366667456o6l34u594t3', '2021-02-18', '16:43:31'),
(51, '589595', '161366667456o6l34u594t3', '1613835852d1o9a7uo076n8729l', '2021-02-20', '15:41:00'),
(52, '190201', '161366667456o6l34u594t3', '16160937172uoo0a4s9n41b41nu0lt', '2021-02-24', '17:31:07'),
(53, '563173', '161366667456o6l34u594t3', '16163277030i1o6o7b92l3041maj', '2021-03-21', '11:53:41');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_hash_id` varchar(225) NOT NULL,
  `image_1` text NOT NULL,
  `image_hash_id` varchar(225) NOT NULL,
  `date_created` text NOT NULL,
  `time_created` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `asset_hash_id`, `image_1`, `image_hash_id`, `date_created`, `time_created`) VALUES
(24, '12345uhygfds1234567', 'public/jsimages/A_few_words_about_us_img_1.jpg', '0', '', '00:00:00'),
(25, '12345uhygfds1234567', 'public/jsimages/A_few_words_about_us_img_2.jpg', '0', '', '00:00:00'),
(26, '8968450389j90819542abn72irnd', 'health3.jpg', '0', '', '00:00:00'),
(30, '6596109378j90819542abn72irnd', 'health4.jpg', '0', '', '00:00:00'),
(31, '6596109378j90819542abn72irnd', 'blog3.jpg', '0', '', '00:00:00'),
(32, '6596109378j90819542abn72irnd', 'blog6.jpg', '0', '', '00:00:00'),
(33, '5279239355j90819542abn72irnd', 'health1.jpg', '0', '', '00:00:00'),
(34, '5279239355j90819542abn72irnd', 'blog3.jpg', '0', '', '00:00:00'),
(35, '5279239355j90819542abn72irnd', 'blog2.jpg', '0', '', '00:00:00'),
(36, '5279239355j90819542abn72irnd', 'blog4.jpg', '0', '', '00:00:00'),
(37, '6596109378j90819542abn72irnd', 'blog5.jpg', '0', '', '00:00:00'),
(38, '6596109378j90819542abn72irnd', 'blog6.jpg', '0', '', '00:00:00'),
(39, '6596109378j90819542abn72irnd', 'health2.jpg', '0', '', '00:00:00'),
(44, '5279239355j90819542abn72irnd', 'blog1.jpg', '0', '', '00:00:00'),
(49, '1613779928_54712', 'https://thehealthcityonline.com/uploads/161377992847616filename.jpg', 'IMG_1613779928', '2021-02-20', '00:12:08'),
(50, '1613779928_54712', 'https://thehealthcityonline.com/uploads/161377992893636filename.jpg', 'IMG_1613779928', '2021-02-20', '00:12:08'),
(51, '1613779928_54712', 'https://thehealthcityonline.com/uploads/161377992817284filename.jpg', 'IMG_1613779928', '2021-02-20', '00:12:08'),
(52, '1613779928_54712', 'https://thehealthcityonline.com/uploads/161377992832691filename.jpg', 'IMG_1613779928', '2021-02-20', '00:12:08'),
(53, '1613779928_54712', 'https://thehealthcityonline.com/uploads/161377992865874filename.jpg', 'IMG_1613779928', '2021-02-20', '00:12:08'),
(54, '5726527404j90819542abn72irnd', 'https://thehealthcityonline.com/uploads/161658839198679filename.jpg', 'IMG_16165883915933206', '2021-03-24', '12:19:51'),
(55, '1616588718_85390', 'https://thehealthcityonline.com/uploads/161658871988441filename.jpg', 'IMG_1616588719', '2021-03-24', '12:25:19'),
(61, '1617992959_72961', 'https://thehealthcityonline.com/uploads/161799306991623filename.jpg', 'IMG_16179930694569199', '2021-04-09', '18:31:09');

-- --------------------------------------------------------

--
-- Table structure for table `panel_about`
--

CREATE TABLE `panel_about` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_title` varchar(50) DEFAULT NULL,
  `text_content` text,
  `text_description` text,
  `text_arr` text,
  `image_2` varchar(225) DEFAULT NULL,
  `visibility` varchar(10) DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_about`
--

INSERT INTO `panel_about` (`id`, `input_title`, `text_content`, `text_description`, `text_arr`, `image_2`, `visibility`, `hash_id`, `date_created`, `time_created`) VALUES
(1, 'Justinches Digital', 'We are a one-stop digital shop serving clients in multiple industries and sectors. With deep roots in branding, PR, and Tech, we offer a robust and potent cocktail of value-adding digital services, targeted at placing you at the forefront of your industry', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'M: Master,I: Innovate,N: Narrate,D: Deploy', NULL, 'show', '12345uhygfds1234567', '2022-03-28', '09:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `panel_blog`
--

CREATE TABLE `panel_blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_title` varchar(255) DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `text_content` text,
  `image_1` varchar(255) DEFAULT NULL,
  `visibility` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_blog`
--

INSERT INTO `panel_blog` (`id`, `input_title`, `hash_id`, `text_content`, `image_1`, `visibility`, `date_created`, `time_created`) VALUES
(1, 'get some weight off feet.', '876trkjhgfd56u8876h', 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'public/jsimages/61f6fd43d9cb4.jpg', 'show', '2022-03-28', '08:00:00'),
(2, 'Demo Blog Post.', '876trkjhgfd56u8876hssss', 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'public/jsimages/61f6c13f617b4.jpg', 'show', '2022-03-28', '08:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `panel_contact`
--

CREATE TABLE `panel_contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_address` varchar(100) DEFAULT NULL,
  `input_phone_number` varchar(50) DEFAULT NULL,
  `input_email` varchar(50) DEFAULT NULL,
  `visibility` varchar(10) DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_contact`
--

INSERT INTO `panel_contact` (`id`, `input_address`, `input_phone_number`, `input_email`, `visibility`, `hash_id`, `date_created`, `time_created`) VALUES
(1, 'Maryland, USA', '+14438688920', 'info@vibecitybooths.com', 'show', '2345hgfd234', '2022-03-28', '08:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `panel_home_portfolio`
--

CREATE TABLE `panel_home_portfolio` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_title` varchar(50) DEFAULT NULL,
  `input_subtitle` varchar(50) DEFAULT NULL,
  `image_1` varchar(225) DEFAULT NULL,
  `visibility` varchar(10) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_home_portfolio`
--

INSERT INTO `panel_home_portfolio` (`id`, `input_title`, `input_subtitle`, `image_1`, `visibility`, `date_created`, `time_created`, `hash_id`) VALUES
(1, 'Demo Portfolio 1', 'Subtitle', 'public/jsimages/61f6fd43d9cb4.jpg', 'show', '2022-03-28', '08:00:00', 'qwert7654356799098'),
(2, 'Demo Portfolio 2', 'Subtitle', 'public/jsimages/61f6fd43d9cb4.jpg', 'show', '2020-04-04', '09:11:50', '2345ytfds23'),
(3, 'Demo Portfolio 3', 'Subtitle', 'public/jsimages/61f6fd43d9cb4.jpg', 'show', '2022-03-28', '08:00:00', 'qwert7654356799098wwwwwwww'),
(4, 'Demo Portfolio 4', 'Subtitle', 'public/jsimages/61f6fd43d9cb4.jpg', 'show', '2020-04-04', '09:11:50', '2345ytfds23eeeeeeeeee'),
(5, 'Demo Portfolio 5', 'Subtitle', 'public/jsimages/61f6fd43d9cb4.jpg', 'show', '2022-03-28', '08:00:00', 'qwert7654356799098ddsd'),
(6, 'Demo Portfolio 6', 'Subtitle', 'public/jsimages/61f6fd43d9cb4.jpg', 'show', '2020-04-04', '09:11:50', '2345ytfds23ffggfg'),
(7, 'Demo Portfolio 7', 'Subtitle', 'public/jsimages/61f6fd43d9cb4.jpg', 'show', '2022-03-28', '08:00:00', 'qwert7654356799098wwwwwwww1212'),
(8, 'Demo Portfolio 8', 'Subtitle', 'public/jsimages/61f6fd43d9cb4.jpg', 'show', '2020-04-04', '09:11:50', '2345ytfds23eeeeeeeeee1212');

-- --------------------------------------------------------

--
-- Table structure for table `panel_service`
--

CREATE TABLE `panel_service` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_title` varchar(30) DEFAULT NULL,
  `icon_icon` varchar(20) DEFAULT NULL,
  `text_description` text,
  `visibility` varchar(10) DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_service`
--

INSERT INTO `panel_service` (`id`, `input_title`, `icon_icon`, `text_description`, `visibility`, `hash_id`, `date_created`, `time_created`) VALUES
(1, 'Identity & Experience Design', 'icon_easel', 'Suspendisse suscipit accumsan enim non euismod. Vivamus fringilla', 'show', 'qwe12340987oiuykjh', '2022-03-28', '04:18:12'),
(2, 'Web and Mobile', 'icon_globe', 'Suspendisse suscipit accumsan enim non euismod. Vivamus fringilla', 'show', 'jhg4567sdf2398', '2022-03-27', '05:22:08'),
(3, 'Analysis of financial', 'icon_genius', 'Suspendisse suscipit accumsan enim non euismod. Vivamus fringilla', 'show', 'qwe12340987oiuykjh23323', '2022-03-28', '04:18:12'),
(4, 'Identity & Experience Design', 'icon_easel', 'Suspendisse suscipit accumsan enim non euismod. Vivamus fringilla', 'show', 'jhg4567sdf2398vcxs', '2022-03-27', '05:22:08'),
(5, 'Image & Content', 'icon_globe', 'Suspendisse suscipit accumsan enim non euismod. Vivamus fringilla', 'show', 'jhg4567sdf2398assdffd', '2022-03-27', '05:22:08'),
(6, 'Analytics & Business Solutions', 'icon_genius', 'Suspendisse suscipit accumsan enim non euismod. Vivamus fringilla', 'show', 'qwe12340987oiuys333r44kjh23323', '2022-03-28', '04:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `panel_slider`
--

CREATE TABLE `panel_slider` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_title` varchar(50) DEFAULT NULL,
  `input_text` varchar(100) DEFAULT NULL,
  `image_1` varchar(225) DEFAULT NULL,
  `visibility` varchar(10) DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_slider`
--

INSERT INTO `panel_slider` (`id`, `input_title`, `input_text`, `image_1`, `visibility`, `hash_id`, `date_created`, `time_created`) VALUES
(1, 'Shift. Sell. Scale', 'Innovative Solutions that thrives.', 'public/assets/js/slider-images/gradienta-gwE9vXSi7Xw-unsplash.jpg', 'show', 'efe3e31r3efe3f3e', '2022-03-28', '08:00:00'),
(2, 'Connect Collaborative Convert', 'Collaborative engagement that guarantees competitve advantage', 'public/assets/js/slider-images/gradienta-BgrRH1_ZI5Y-unsplash.jpg', 'show', 'mjm68k87j76uk67uj', '2020-04-04', '09:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `panel_team`
--

CREATE TABLE `panel_team` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_name` varchar(50) DEFAULT NULL,
  `input_position` varchar(50) DEFAULT NULL,
  `image_1` varchar(225) DEFAULT NULL,
  `hash_id` varchar(255) DEFAULT NULL,
  `visibility` varchar(10) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_team`
--

INSERT INTO `panel_team` (`id`, `input_name`, `input_position`, `image_1`, `hash_id`, `visibility`, `date_created`, `time_created`) VALUES
(1, 'Demo Team Member 1', 'CEO', 'public/jsimages/61f6f7132cc2c.jpg', '2345asdfghxcvbnm', 'show', '2022-06-17', '07:12:24'),
(2, 'Demo Team Member 2', 'Project Manager', 'public/jsimages/61f6b9f968d74.jpg', '0987oiuysdfgh12345', 'show', '2022-02-18', '07:00:00'),
(3, 'Demo Team Member 3', 'IT Lead', 'public/jsimages/61f6c1df99d8b.jpg', '2345asdfghxcvbnm1234r87', 'show', '2022-06-17', '07:12:24'),
(4, 'Demo Team Member 4', 'Programmes Lead', 'public/jsimages/61f6f88293141.jpg', '0987oiuysdfgh12345qwert', 'show', '2022-02-18', '07:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `panel_testimonial`
--

CREATE TABLE `panel_testimonial` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_name` varchar(50) DEFAULT NULL,
  `input_organization` varchar(50) DEFAULT NULL,
  `text_testimony` text,
  `visibility` varchar(10) DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_testimonial`
--

INSERT INTO `panel_testimonial` (`id`, `input_name`, `input_organization`, `text_testimony`, `visibility`, `hash_id`, `date_created`, `time_created`) VALUES
(1, 'Demo 1', 'Demo Organization 1', 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed doeiusmod tempor incididunt ut labore et', 'show', '123jhgfd98765', '2022-03-28', '11:33:49'),
(2, 'Demo 2', 'Demo Organization 2', 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed doeiusmod tempor incididunt ut labore et', 'show', '98765432sdfghjk', '2020-04-04', '04:11:00');

-- --------------------------------------------------------

--
-- Table structure for table `read_newsletter`
--

CREATE TABLE `read_newsletter` (
  `id` int(10) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `input_email` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `read_newsletter`
--

INSERT INTO `read_newsletter` (`id`, `hash_id`, `input_email`, `date_created`, `time_created`) VALUES
(1, '161376969886295', 'earltbam@yahoo.com', '2021-02-19', '21:21:38'),
(2, '161404402252628', 'brandon.scarborough1@gmail.com', '2021-02-23', '01:33:42'),
(3, '161433181998692', 'patrick.brookem@elginqs.com', '2021-02-26', '09:30:19'),
(4, '161466707987612', 'kakaneg4@gmail.com', '2021-03-02', '06:37:59'),
(5, '161522993975071', 'tmacleod@boxmanstudios.com', '2021-03-08', '18:58:59'),
(6, '161641854342098', 'papanz_66@hotmail.com', '2021-03-22', '13:09:03'),
(7, '161674839238103', 'ward@eldersoft.ca', '2021-03-26', '08:46:32'),
(8, '161712003583940', 'tawand22@yahoo.com', '2021-03-30', '16:00:35'),
(9, '161713352455505', 'qhoneypooh@hotmail.com', '2021-03-30', '19:45:24'),
(10, '161822400884435', 'marceclarembaux@gmail.com', '2021-04-12', '10:40:08'),
(11, '161848387916937', 'oghesanileonard@gmail.com', '2021-04-15', '10:51:19'),
(12, '161850847254902', 'adeleyeadeyemijoshua@gmail.com', '2021-04-15', '17:41:12'),
(13, '161851225740629', 'udokafamaechi@gmail.com', '2021-04-15', '18:44:17'),
(14, '161851681757974', 'olabintanodunola@yahoo.com', '2021-04-15', '20:00:17'),
(15, '161851991530522', 'gabrieloke211@gmail.com', '2021-04-15', '20:51:55'),
(16, '161852087461409', 'Michael_ibk@yahoo.com', '2021-04-15', '21:07:54'),
(17, '161855205065289', '', '2021-04-16', '05:47:30'),
(18, '161856117089744', 'ayomadeadeyemi5@gmail.com', '2021-04-16', '08:19:30'),
(19, '161857214285879', 'agudapaul150@gmail.com', '2021-04-16', '11:22:22'),
(20, '161868813225303', 'blessingitunu.ajayi@gmail.com', '2021-04-17', '19:35:32'),
(21, '161954842554261', 'adebiyibisola2020@gmail.com', '2021-04-27', '18:33:45'),
(22, '161976111151057', 'toswell007@yahoo.com', '2021-04-30', '05:38:31'),
(23, '162005440665619', 'adedoyinadegun@yahoo.com', '2021-05-03', '15:06:46'),
(24, '162015202850749', 'mail@toneandcaz.plus.com', '2021-05-04', '18:13:48'),
(25, '162024885936580', 'Turner_Nikolaus2@yahoo.com', '2021-05-05', '21:07:39'),
(26, '162211104072189', 'ferry@im-mo.nl', '2021-05-27', '10:24:00'),
(27, '162231155492922', 'hefveakins608@gmail.com', '2021-05-29', '18:05:54'),
(28, '162317526195867', 'jomoniyi2@gmail.com', '2021-06-08', '18:01:01'),
(29, '162330757049581', 'awaseem@live.ca', '2021-06-10', '06:46:10'),
(30, '164853561976064', 'healthcity@gmail.com', '2022-03-29', '06:33:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_auth`
--
ALTER TABLE `admin_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_about`
--
ALTER TABLE `panel_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_blog`
--
ALTER TABLE `panel_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_contact`
--
ALTER TABLE `panel_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_home_portfolio`
--
ALTER TABLE `panel_home_portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_service`
--
ALTER TABLE `panel_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_slider`
--
ALTER TABLE `panel_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_team`
--
ALTER TABLE `panel_team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_testimonial`
--
ALTER TABLE `panel_testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `read_newsletter`
--
ALTER TABLE `read_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `admin_auth`
--
ALTER TABLE `admin_auth`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `panel_about`
--
ALTER TABLE `panel_about`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `panel_blog`
--
ALTER TABLE `panel_blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `panel_contact`
--
ALTER TABLE `panel_contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `panel_home_portfolio`
--
ALTER TABLE `panel_home_portfolio`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `panel_service`
--
ALTER TABLE `panel_service`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `panel_slider`
--
ALTER TABLE `panel_slider`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `panel_team`
--
ALTER TABLE `panel_team`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `panel_testimonial`
--
ALTER TABLE `panel_testimonial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `read_newsletter`
--
ALTER TABLE `read_newsletter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
