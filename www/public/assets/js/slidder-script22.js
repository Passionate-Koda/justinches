const slideContainer = document.querySelector('.slider');
const sliderText = document.querySelector('.slider--text');
const sliderTitle = document.querySelector('.slider--title');
const btnLeft = document.querySelector('.slider__btn-left');
const btnRight = document.querySelector('.slider__btn-right');

var arr = {
  "data": "panel_slider",
  "where": {
    "visibility": 'show'
  }
}
ajaxPost("/read", arr, function(err, response){
  console.log(response);
  var res = JSON.parse(response);
  console.log(res.data);
  const sliderImages = res.data;


let slideCounter = 1;

const startSlider = () => {
  slideContainer.style.backgroundImage = `url(${sliderImages[0].image_1})`;
  sliderText.innerHTML = sliderImages[0].input_text;
  sliderTitle.innerHTML = sliderImages[0].input_title;

};

btnRight.addEventListener('click', function() {
  if (slideCounter === sliderImages.length - 1) {
    slideContainer.style.backgroundImage = `url(${sliderImages[0].image_1})`;
    sliderText.innerHTML = sliderImages[0].input_text;
    slideCounter = -1;

    slideContainer.classList.add('fadeIn');
    setTimeout(() => {
      slideContainer.classList.remove('fadeIn');
    }, 1000);
  }

  slideContainer.style.backgroundImage = `url(${sliderImages[slideCounter + 1].image_1})`;
  sliderText.innerHTML = sliderImages[slideCounter + 1].input_text;
  sliderTitle.innerHTML = sliderImages[slideCounter + 1].input_title;

  slideCounter++;
  slideContainer.classList.add('fadeIn');
  setTimeout(() => {
    slideContainer.classList.remove('fadeIn');
  }, 1000);

});

btnLeft.addEventListener('click', function() {
  if (slideCounter === 0) {
    slideContainer.style.backgroundImage = `url(${sliderImages[sliderImages.length - 1].image_1})`;
    sliderText.innerHTML = sliderImages[sliderImages.length - 1].input_text;
    slideCounter = sliderImages.length;
    slideContainer.classList.add('fadeIn');
    setTimeout(() => {
      slideContainer.classList.remove('fadeIn');
    }, 1000);
  }

  slideContainer.style.backgroundImage = `url(${sliderImages[slideCounter - 1].image_1})`;
  sliderText.innerHTML = sliderImages[slideCounter - 1].input_text;
  sliderTitle.innerHTML = sliderImages[slideCounter + 1].input_title;
  slideCounter--;
  slideContainer.classList.add('fadeIn');
  setTimeout(() => {
    slideContainer.classList.remove('fadeIn');
  }, 1000);
});
document.addEventListener('DOMContentLoaded', startSlider);
});
