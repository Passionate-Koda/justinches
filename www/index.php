<?php
ob_start();
// $site_name = "Justinches";
// $site_email = "info@vibecitybooths.com";
// $site_description = "We are a one-stop digital shop serving clients in multiple industries and sectors. Our aim is to enable you to serve your clients better, whether it be through a user-friendly mobile app, an optimized interactive website, or a viral social campaign the Customer always wins at Justinches!";
// $logo_directory = "/public/jsimages/justinches_logo_website.png";
$dummy = "dummy.png";
$fbid = "2213158278782711";
session_start();
// die("Critical Maintenance in progress");
#Define App Path
define("D_PATH", dirname(dirname(__FILE__)));
CONST APP_PATH = D_PATH."/v1";
#load database
#load Controllers(functions)
include D_PATH."/.env/config.php";
#load routes
require APP_PATH."/models/model.php";
require APP_PATH."/controllers/controller.php";
require APP_PATH."/auth/auth_controller/controller.php";
#load routes
// require APP_PATH."/routes/router.php";


$websiteInfo = selectContent($conn, "settings_website_info", ['visibility' => 'show']);
$logo2 = selectContent($conn, "settings_website_logo_2", ['visibility' => 'show']);
// $fetchFavicon = selectContent($conn, "read_favicon", ['visibility' => 'show']);


// $_SESSION['color'] = "green";

$site_name = $websiteInfo[0]['input_name'];
$site_email = $websiteInfo[0]['input_email'];
$site_phone = $websiteInfo[0]['input_phone_number'];
$site_address = $websiteInfo[0]['input_address'];
$site_email2 = $websiteInfo[0]['input_email_2'];
$site_phone2 = $websiteInfo[0]['input_phone_2'];
$site_address2 = $websiteInfo[0]['input_address_2'];
$fbLink = $websiteInfo[0]['input_facebook'];
$igLink = $websiteInfo[0]['input_instagram'];
$linkedinLink = $websiteInfo[0]['input_linkedin'];
$twitterLink = $websiteInfo[0]['input_twitter'];
// $behanceLink = $websiteInfo[0]['input_behance'];
// $dribbbleLink = $websiteInfo[0]['input_dribbble'];
$site_description = $websiteInfo[0]['text_description'];
$logo_directory = $websiteInfo[0]['image_1'];
$logo_directory2 = $logo2[0]['image_1'];
// $favicon = $fetchFavicon[0]['image_1'];





include APP_PATH."/routes/router.php";
include APP_PATH."/ajax/ajax_router/router.php";
include APP_PATH."/payment/payment_router/router.php";
include APP_PATH."/auth/auth_router/router.php";
include APP_PATH."/routes/ajax_router.php";
include APP_PATH."/routes/admin_router.php";

 ?>
